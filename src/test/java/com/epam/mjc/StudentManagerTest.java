package com.epam.mjc;

import static org.junit.Assert.*;

import org.junit.Test;


public class StudentManagerTest {
  StudentManager manager = new StudentManager();

  @Test(expected = IllegalArgumentException.class)
  public void findNotValid() throws IllegalArgumentException {
    manager.find(1000);
  }

  @Test
  public void findValidStudent() {
    try {
      assertNotNull(manager.find(1));
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testExceptionMessage() {
    try {
        assertNotNull(manager.find(1000));
    } catch (IllegalArgumentException e) {
      assertEquals("Could not find student with ID 1000", e.getMessage());
    }
  }

}